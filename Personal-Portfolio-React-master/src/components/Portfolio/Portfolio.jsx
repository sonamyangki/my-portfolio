import React, { useContext } from "react";
import "./Portfolio.css";
import { Swiper, SwiperSlide } from "swiper/react"
import "swiper/css";
import fpp from '../../img/fpp.png'
import gyelrab from "../../img/gyelrap1.png";
import secondhand from "../../img/secondhand.png";
import medicalreport from "../../img/medicalreport1.png";
import goldprice from "../../img/goldpricepre.png"
import { themeContext } from "../../Context";
import ClickableSwiperSlide from "./clickable";

const Portfolio = () => {
  const theme = useContext(themeContext);
  const darkMode = theme.state.darkMode;
  return (
    <div className="portfolio" id="portfolio">
      {/* heading */}
      <span 
        className="my_portfolio"
        style={{color: darkMode?'white': '',
        fontSize: "35px", // Adjust the font size for mobile
        display: "inline-block", // Ensure consistent line height
        }}>
        Recent Projects
      </span>
      <span
        className="my_portfolio"
        style={{
        fontSize: "35px", // Adjust the font size for mobile
        display: "inline-block", // Ensure consistent line height
        }}
        >Portfolio</span>

      {/* slider */}
      <Swiper
        spaceBetween={30}
        slidesPerView={3}
        grabCursor={true}
        className="portfolio-slider"
      >
        <SwiperSlide>
          <ClickableSwiperSlide 
            imageSrc={fpp} 
            gitlabLink="https://flight-price-prediction-frontend.vercel.app/"

          />
        </SwiperSlide>

        <SwiperSlide>
          <ClickableSwiperSlide imageSrc={secondhand} gitlabLink="https://gitlab.com/sonamyangki/second-hand-shop" />
          {/* <img src={secondhand} alt="" /> */}
        </SwiperSlide>
        
        <SwiperSlide>
          <ClickableSwiperSlide imageSrc={gyelrab} gitlabLink="https://gitlab.com/12200060.gcit/medical-report" />
        </SwiperSlide>
        
        <SwiperSlide>
          <ClickableSwiperSlide imageSrc={medicalreport} gitlabLink="https://gitlab.com/sonamyangki/medical-report_prj202" />
        </SwiperSlide>

        <SwiperSlide>
          <ClickableSwiperSlide imageSrc={goldprice} gitlabLink="https://gitlab.com/sonamyangki/its307_group5 "/>
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default Portfolio;
