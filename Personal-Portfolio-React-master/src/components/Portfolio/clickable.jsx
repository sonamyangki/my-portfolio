import React from 'react';
import { SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css'; // Import Swiper styles

const ClickableSwiperSlide = ({ imageSrc, gitlabLink }) => {
  const handleClick = () => {
    window.location.href = gitlabLink; // Redirect to the provided GitLab link
  };

  return (
    <SwiperSlide>
      <div onClick={handleClick} style={{ cursor: 'pointer' }}>
        <img src={imageSrc} alt="" />
      </div>
    </SwiperSlide>
  );
};

export default ClickableSwiperSlide;
