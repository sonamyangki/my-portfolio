import React, { useContext } from "react";
import "./Services.css";
import Card from "../Card/Card";
import design from "../../img/designb.png";
import developers from "../../img/developer.png";
import uiux from "../../img/ux-designer.png";
import { themeContext } from "../../Context";
import { motion } from "framer-motion";
import Resume from './resume.pdf';

const Services = () => {
  // context
  const theme = useContext(themeContext);
  const darkMode = theme.state.darkMode;

  // transition
  const transition = {
    duration: 1,
    type: "spring",
  };

  return (
    <div className="services" id="services">
      {/* left side */}
      <div className="awesome">
        {/* dark mode */}
        <span style={{ color: darkMode ? "white" : "" }}></span>
        <span
          className="my_skills"
          style={{
          color: darkMode ? "white" : "",
          fontSize: "35px", // Adjust the font size for mobile
          display: "inline-block", // Ensure consistent line height
        }}
        > My Skills</span>
        <spane>
        I specialize in UI/UX design and web/mobile app development, crafting engaging <br/> experiences, creating captivating graphics, and enhancing brand identity.
        </spane>
        <a href={Resume} download>
          <button className="button s-button">Download CV</button>
        </a>
        <div className="blur s-blur1" style={{ background: "#ABF1FF94" }}></div>
      </div>
      {/* right */}
      <div className="cards">
        {/* first card */}
        <motion.div
          initial={{ left: "25rem" }}
          whileInView={{ left: "14rem" }}
          transition={transition}
          
        >
          <Card
            emoji={design}
            heading={"Design"}
            detail={"Figma, Sketch, Photoshop, Adobe Illustrator, Adobe InDesign"}
            additionalInfo={" I excel in design, utilizing tools like Figma, Adobe Illustrator, and Adobe InDesign to craft visually captivating and user-centric creations."}
          />
        </motion.div>
        {/* second card */}
        <motion.div
          initial={{ left: "-11rem", top: "12rem" }}
          whileInView={{ left: "-4rem" }}
          transition={transition}
        >
          <Card
            emoji={developers}
            heading={"Developer"}
            detail={"Html, Css, JavaScript, React, Nodejs, Express"}
            additionalInfo={'Cultivated in HTML, CSS, JavaScript, React, Node.js, and Express, my skill set is primed for development tasks, welcoming inquiries from those seeking a capable developer.'}
          />
        </motion.div>
        {/* 3rd */}
        <motion.div
          initial={{ top: "19rem", left: "25rem" }}
          whileInView={{ left: "12rem" }}
          transition={transition}
        >
          <Card
            emoji={uiux}
            heading={"UI/UX"}
            detail={
              "Figma, Marvel"
            }
            additionalInfo={'Proficient in the art of UI/UX design, my expertise guarantees seamless user experiences, and those seeking innovative design solutions are welcome to reach out.'}
            color="rgba(252, 166, 31, 0.45)"
          />
        </motion.div>
        <div
          className="blur s-blur2"
          style={{ background: "var(--purple)" }}
        ></div>
      </div>
    </div>
  );
};

export default Services;
