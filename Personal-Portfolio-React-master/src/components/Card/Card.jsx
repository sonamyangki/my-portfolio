
// import React from "react";
// import "./Card.css";

// const Card = ({emoji, heading, detail, color}) => {
//   return (
//     <div className="card" style={{borderColor: {color}}}> 
//       <img src={emoji} alt="" />
//       <span>{heading}</span>
//       <span>{detail}</span>
//       <button className="c-button">LEARN MORE</button>
//     </div>
//   );
// };

// export default Card;

import React, { useState } from "react";
import "./Card.css";

const Card = ({ emoji, heading, detail, additionalInfo, color }) => {
  const [showMore, setShowMore] = useState(false);

  const toggleShowMore = () => {
    setShowMore(!showMore);
  };

  return (
    <div className="card" style={{ borderColor: color }}>
      <img src={emoji} alt="" />
      {!showMore && (
        <>
          <span>{heading}</span>
          <span>{detail}</span>
        </>
      )}
      {showMore && (
        <div className="more-info">
          {additionalInfo}
        </div>
      )}
      <button className="c-button" onClick={toggleShowMore}>
        {showMore ? "SHOW LESS" : "LEARN MORE"}
      </button>
    </div>
  );
};

export default Card;
