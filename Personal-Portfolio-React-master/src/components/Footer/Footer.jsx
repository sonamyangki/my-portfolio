import React from "react";
import "./Footer.css";
import Wave from "../../img/wave.png";
import Insta from "@iconscout/react-unicons/icons/uil-instagram";
import Facebook from "@iconscout/react-unicons/icons/uil-facebook";
import { UilWhatsapp } from '@iconscout/react-unicons';

const Footer = () => {
  return (
    <div className="footer">
      <img src={Wave} alt="" style={{ width: "100%" }} />
      <div className="f-content">
        <span>12200085.gcit@rub.edu.bt</span>
        <div className="f-icons">
          <a href="https://instagram.com/yangkey567?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D" target="_blank" rel="noopener noreferrer">
            <Insta color="white" size="3rem" />
          </a>
          
          <a href="https://www.facebook.com/sonam.yangki.5492216?mibextid=ZbWKwL" target="_blank" rel="noopener noreferrer">
            <Facebook color="white" size="3rem" />
          </a>
          <a href="https://whatsapp.com/dl/" target="_blank" rel="noopener noreferrer">
            <UilWhatsapp color="white" size="3rem" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
