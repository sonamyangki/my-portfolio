import React, { useContext } from "react";
import "./Intro.css";
import boy from "../../img/girl.png";
import star from "../../img/star.png";
// import FloatinDiv from "../FloatingDiv/FloatingDiv";
import { themeContext } from "../../Context";
// import { motion } from "framer-motion";
import { Link } from "react-scroll";
const Intro = () => {
  // Transition
  // const transition = { duration: 2, type: "spring" };

  // context
  const theme = useContext(themeContext);
  const darkMode = theme.state.darkMode;

  return (
    <div className="Intro" id="Intro">
      {/* left name side */}
      <div className="i-left">
        <div className="i-name">
          {/* yahan change hy darkmode ka */}
          <span
            className="intro-text"
            style={{
            color: darkMode ? "white" : "",
            fontSize: "35px", // Adjust the font size for mobile
            display: "inline-block", // Ensure consistent line height
          }}
        >
          Hello there! I Am
        </span>
          {/* <span className="intro-text" style={{ color: darkMode ? "white" : "" }}>Hello there! I Am</span> */}
          <span 
            className="name-text"
            style={{
            color: darkMode ? "white" : "",
            fontSize: "45px", // Adjust the font size for mobile
            display: "inline-block", // Ensure consistent line height
          }}
            >
            Sonam Yangki</span>
          <span>
          👋  An aspiring tech explorer with a zest for innovation. <br/><br/>
          
          🚀 Currently navigating the fascinating world of Information Technology, I'm on a mission to transform concepts into captivating digital experiences.<br/><br/>

          🌟 With code as my paintbrush and pixels as my canvas, I craft user-friendly interfaces that dance seamlessly across screens. From brainstorming in bits to designing with bytes, I find sheer joy in bringing ideas to life.<br/><br/>

          🔍 Beyond the pixels, you'll find me unraveling the mysteries of algorithms, diving into databases, and collaborating with fellow tech enthusiasts. Every line of code is a step toward unraveling the endless possibilities of the digital realm.<br/><br/>

          🌐 Welcome to my digital playground, where creativity knows no bounds and innovation is a way of life. Join me on this extraordinary journey as we redefine what's possible in the world of technology.<br/><br/>

          Stay curious, dream big, and let's build the future together!<br/><br/>

          Cheers,<br/>
          Sonam Yangki
          </span>
        </div>
          <Link to="contact" smooth={true} spy={true}>
            <button className="button i-button">Hire me</button>
          </Link>
        </div>
      {/* right image side */}
      
      <div className="i-right">
        <div className="image-container">
          <img src={boy} alt="" className="girl" />
          <img src={star} alt="" className="star-image" />
        </div>
        {/* animation */}
        {/* <motion.img
          initial={{ left: "-36%" }}
          whileInView={{ left: "-24%" }}
          transition={transition}
          src={glassesimoji}
          alt=""
        /> */}

        
        {/* <motion.div
          initial={{ top: "-4%", left: "74%" }}
          whileInView={{ left: "68%" }}
          transition={transition}
          className="floating-div"
        >
          <FloatinDiv img={star} text1="Front End" text2="Developer" />
        </motion.div> */}

        {/* animation */}
        {/* <motion.div
          initial={{ left: "9rem", top: "18rem" }}
          whileInView={{ left: "0rem" }}
          transition={transition}
          className="floating-div"
        > */}
          {/* floatinDiv mein change hy dark mode ka */}
          {/* <FloatinDiv img={thumbup} text1="Good At" text2="Designing" /> */}
        {/* </motion.div> */}

        <div className="blur" style={{ background: "rgb(238 210 255)" }}></div>
        <div
          className="blur"
          style={{
            background: "#C1F5FF",
            top: "17rem",
            width: "21rem",
            height: "11rem",
            left: "-9rem",
          }}
        ></div>
      </div>
    </div>
  );
};

export default Intro;
