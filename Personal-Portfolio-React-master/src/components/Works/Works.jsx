import React, { useContext } from "react";
import "./Works.css";
import html from "../../img/html.png"
import react from "../../img/react.png";
import ai from "../../img/ai.png";
import ps from "../../img/Photoshop.png";
import node from "../../img/node.png";
import { themeContext } from "../../Context";
import { motion } from "framer-motion";
import {Link} from 'react-scroll'
const Works = () => {
  // context
  const theme = useContext(themeContext);
  const darkMode = theme.state.darkMode;

  // transition
  return (
    <div className="works" id="works">
      {/* left side */}
      <div className="w-left">
        <div className="awesome">
          {/* dark Mode */}
          <span 
            className="my_services"
            style={{ color: darkMode ? "white" : "",
            fontSize: "35px", // Adjust the font size for mobile
            display: "inline-block", // Ensure consistent line height
            }}
            
            >
            Works for All these
          </span>
          <span
            className="my_services"
            style={{ 
            fontSize: "35px", // Adjust the font size for mobile
            display: "inline-block", // Ensure consistent line height
            }}
          >Brands & Clients</span>
          <spane>
          Though my journey has yet to include collaborations with major brands, my passion for design has driven me to successfully complete multiple projects during my degree. With a strong foundation and a hunger for growth, I eagerly anticipate the opportunity to contribute my skills to esteemed brands and clients in the future.
          </spane>
          <Link to="contact" smooth={true} spy={true}>
            <button className="button s-button">Hire Me</button>
          </Link>
          <div
            className="blur s-blur1"
            style={{ background: "#ABF1FF94" }}
          ></div>
        </div>

        {/* right side */}
      </div>
      <div className="w-right">
        <motion.div
          initial={{ rotate: 45 }}
          whileInView={{ rotate: 0 }}
          viewport={{ margin: "-40px" }}
          transition={{ duration: 3.5, type: "spring" }}
          className="w-mainCircle"
        >
          <div className="w-secCircle">
            <img src={html} alt="" />
          </div>
          <div className="w-secCircle">
            <img src={react} alt="" />
          </div>
          <div className="w-secCircle">
            <img src={ai} alt="" />
          </div>{" "}
          <div className="w-secCircle">
            <img src={ps} alt="" />
          </div>
          <div className="w-secCircle">
            <img src={node} alt="" />
          </div>
        </motion.div>
        {/* background Circles */}
        <div className="w-backCircle blueCircle"></div>
        <div className="w-backCircle yellowCircle"></div>
      </div>
    </div>
  );
};

export default Works;
